import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;


class GUI extends JFrame {

    private final TextField textField;
    private final TextField textField2;
    private final JButton writeButton;
    private String fileName;
    private String message;

    public GUI() {
        setTitle("Exam");
        textField = new TextField();
        textField.setLocation(0,0);
        textField2 = new TextField();
        textField2.setLocation(100,0);
        writeButton = new JButton("Write");
        writeButton.setLocation(75,50);
        add(textField);
        add(textField2);
        add(writeButton);
        setSize(300, 200);
        textField.setSize(100,50);
        textField2.setSize(100,50);
        writeButton.setSize(100,50);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }

    public static void main(String[] args) {
        GUI gui = new GUI();
        gui.writeButton.addActionListener(e -> {
            gui.fileName = gui.textField.getText();
            gui.message = gui.textField2.getText();
            File myFile = new File("./" + gui.fileName + ".txt");
            try {
                if(myFile.createNewFile()) {
                    FileWriter myWriter = new FileWriter(myFile);
                    myWriter.write(gui.message);
                    myWriter.close();
                } else{
                    System.out.println("Could not create file.");
                }
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        });
    }
}