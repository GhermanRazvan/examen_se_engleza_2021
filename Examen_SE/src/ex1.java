import java.util.ArrayList;

class B {
    public B() {

    }

    public String getParam() {
        return param;
    }

    private String param;

    private ArrayList<D> things;

    private E e;

    public B(E e) {
        this.e = e;
    }


    public void x() {

    }


    public void y() {

    }

}


class U {
    public void fct(B b) {
        System.out.println(b.getParam());
    }

}


class A extends B {


    public A(E e) {
        super(e);
    }
}

class D {


}

class E {

}

class C{
    private B b;
    public C(B b){
        this.b = new B();
    }
}
